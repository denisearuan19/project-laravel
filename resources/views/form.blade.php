<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form</title>
</head>

<body>
    <form action="/about" method="post">
        @csrf

        <label>Nama :</label>
        <input type="text" name="nama"><br>
        <label>Kelas : </label>
        <input type="text" name="kelas"><br>
        <label>Jenis Kelamin :</label>
        <input type="radio" name="jk" value="perempuan">Perempuan
        <input type="radio" name="jk" value="laki-laki">Laki-laki<br>
        <label>Alamat :</label>
        <input type="text" name="alamat"><br>
        <input type="submit" value="masuk">
    </form>
</body>

</html>