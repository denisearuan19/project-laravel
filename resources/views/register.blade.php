<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Register</title>
</head>

<body>
    <h1>Buat Account Baru</h1>
    <h3>Sign Up Form</h3>
    <form action="/kirim" method="POST">
    @csrf
        <label for="fname">First Name:</label><br>
        <input type="text" name="fname"><br>
        <label for="lname">Last Name:</label><br>
        <input type="text" name="lname"><br>
        <label for="jk">Jenis Kelamin :</label><br>
        <input type="radio" name="jk" value="perempuan">Perempuan<br>
        <input type="radio" name="jk" value="laki-laki">Laki-laki<br>
        <input type="radio" name="other" value="other">Other<br>
        <label for="nationality">Nationality</label>
        <select name="nationality" id="nationality">
            <option value="indonesia">Indonesia</option><br>
            <option value="jepang">jepang</option><br>
            <option value="korea">korea</option><br>
        </select><br>
        <label for="language">Language Spoken</label><br>
        <input type="checkbox" name="language" value="Bahasa Indonesia">Bahasa Indonesia<br>
        <input type="checkbox" name="language" value="English">English<br>
        <input type="checkbox" name="language" value="Arabic">Arabic<br>
        <input type="checkbox" name="language" value="Japanese">japanese<br>
        <label for="">Bio :</label><br>
        <textarea id="bio" name="bio" rows="5" cols="25"></textarea><br>
        <input type="submit" value="Submit"><br>
    </form>
</body>

</html>