<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register()
    {

        return view('register');
    }
    public function kirim(Request $request)
    {
        $fname = $request['fname'];
        $lname = $request['lname'];
        $jk = $request['jk'];
        $nationality = $request['nationality'];
        $language = $request['language'];
        $bio = $request['bio'];


        return view('welcome2', compact('fname', 'lname', 'jk', 'nationality', 'language', 'bio'));
    }
}