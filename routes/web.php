<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/master', function () {
    return view('adminlte.master');
});
Route::get('/', function () {
    return view('welcome');
});
Route::get('/home', 'HomeController@home');
Route::get('/register', 'AuthController@register');
Route::post('/kirim', 'AuthController@kirim');
Route::get('/welcome2', 'AuthController@welcome2');
Route::get('/items', function () {
    return view('items.index');
});
Route::get('/items/create', function () {
    return view('items.create');
});
Route::get('/table', function () {
    return view('items.index');
});
Route::get('/data-tables', function () {
    return view('items.create');
});
Route::get('/dashboard', function () {
    return view('items.dashboard');
});

//CRUD Genre
// Route::get('/cast/create', 'CastController@create');
// Route::post('/cast', 'CastController@store');
// Route::get('/cast', 'CastController@index');
// Route::get('/cast/{cast_id}', 'CastController@show');
// Route::get('/cast/{cast_id}/edit', 'CastController@edit');
// Route::put('/cast/{cast_id}', 'CastController@update');
// Route::delete('/cast/{cast_id}', 'CastController@destroy');

//CRUD eloquent
Route::resource('post', 'PostController');
Route::resource('cast', 'CastController');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');